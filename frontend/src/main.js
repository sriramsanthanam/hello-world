// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Raven from 'raven-js'
import RavenVue from 'raven-js/plugins/vue'
import axios from 'axios'

Raven
  .config(process.env.SENTRY_PUBLIC_DSN)
  .addPlugin(RavenVue, Vue)
  .install()

Vue.config.productionTip = false

Vue.prototype.$http = axios.create({
  baseURL: process.env.SERVER_URI,
  timeout: 5000,
  headers: {}
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
