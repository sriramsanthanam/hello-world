import { createLocalVue, shallowMount } from '@vue/test-utils'
import HelloWorld from '@/components/HelloWorld'
import axios from 'axios'
import moxios from 'moxios'

const localVue = createLocalVue()

const http = axios.create({
  baseURL: '',
  timeout: 5000,
  headers: {}
})

localVue.prototype.$http = http

describe('HelloWorld.vue', () => {
  beforeEach(function () {
    moxios.install(http)
  })

  afterEach(function () {
    moxios.uninstall(http)
  })

  it('should render correct contents', async () => {
    moxios.stubRequest('/api/v1/version', {
      status: 200,
      response: {
        version: '0.1'
      }
    })
    const wrapper = shallowMount(HelloWorld, {
      localVue
    })

    moxios.wait(() => {
      wrapper.vm.$nextTick(() => {
        expect(wrapper.vm.$el.querySelector('.hello h1').textContent)
          .to.equal('Welcome to SPX PRJ Configurator')
        expect(wrapper.vm.$el.querySelector('.hello p').textContent)
          .to.equal('Version: 0.1')
      })
    })
  })
})
