*** Settings ***
Documentation     A simple launch app test
Library           SeleniumLibrary    timeout=10

*** Variables ***
${BROWSER}  chrome

*** Test Cases ***
Launch app test
    Open Browser  ${URL}   ${BROWSER}
