'use strict'
module.exports = {
  NODE_ENV: '"production"',
  SENTRY_PUBLIC_DSN: '"' + process.env.SENTRY_PUBLIC_DSN + '"',
  SERVER_URI: '""'
}
