'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  SENTRY_PUBLIC_DSN: '""',
  SERVER_URI: '"http://localhost:8988/"'
})
