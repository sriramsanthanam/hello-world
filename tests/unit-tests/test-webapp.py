import server
import json
import boddle
from bottle import HTTPResponse

def test_webapp_index():
    assert server.dev_enable_cors() == None
    assert server.version() == json.dumps({"version":"0.1"})
    assert server.test() == "test"
    assert server.serve_static('index.html').status_code == 200
    try:
        server.home()
    except HTTPResponse as e:
        assert e.status_code == 302
    
    with boddle.boddle(json={'data':'me'}):
        assert server.post_test() == "test me"
