## SPX PRJ Editor

[![pipeline status](https://megaracgit.ami.com/devnet/spx-prj-editor/badges/master/pipeline.svg)](https://megaracgit.ami.com/devnet/spx-prj-editor/commits/master) [![coverage report](https://megaracgit.ami.com/devnet/spx-prj-editor/badges/master/coverage.svg)](https://megaracgit.ami.com/devnet/spx-prj-editor/commits/master)

Web based PRJ Editor for SPX projects

## Development setup

- Clone this project
- Open this project in VSCode
- Setup Backend
  - Open VSCode Integrated Terminal using ``` Ctrl+` ```
  - Install python requirements
            
            sudo pip install -r requirements.txt
            sudo pip install -r dev_requirements.txt
  - Create config.py with following content

            SENTRY_DSN=''
            APP_PORT=8988
  - Start the python server

            python server.py
  - To perform server only unit tests, do the following in terminal

            nosetests --with-coverage tests/unit-tests/test-webapp.py --cover-package server
- Setup Frontend
  - Open a new Terminal using ``` Ctrl+Shift+` ```
  - Make sure to switch to the new terminal window in the dropdown
  - Goto frontend folder

            cd ./frontend
  - Make sure to install all dependencies

            npm install
  - Start the Vue App 

            npm start
  - Launch the Vue App in a browser instance (Use the application URI shown in console)
  - To perform server independent - UI only - unit tests, do the following in terminal

            npm test

## Backend

#### Framework

- Python 2.7
- Bottle Web Framework

#### Unit Testing

- Nose
- Boddle

Unit testing of this REST API backend app is done by using Nose and boddle test framework

#### Acceptance Testing

- Robot Framework + REST Instance

Acceptance testing of this REST API backend is done by robot framework and rest instance


## Frontend 

#### Framework

- Vue.JS 2.0
- Bootstrap template

#### Unit Testing

- Karma

Unit testing of this VUE.JS frontend app is done by using Karma

#### Acceptance Testing

- Robot Framework + Selenium
- PhantomJS headless

Acceptance testing of this VUE.JS frontend is done by Selenium through robot framework on PhantomJS browser
