from bottle import hook, get, post, request, run, static_file, redirect, route, response
from json import dumps

import bottle

from raven import Client
from raven.contrib.bottle import Sentry

from config import SENTRY_DSN, APP_PORT

app = bottle.app()
app.catchall = False

client = Client(SENTRY_DSN)
app = Sentry(app, client)

_allow_origin = '*'
_allow_methods = 'PUT, GET, POST, DELETE, OPTIONS'
_allow_headers = 'Authorization, Origin, Accept, Content-Type, X-Requested-With'


@hook('after_request')
def dev_enable_cors():
    '''Add headers to enable CORS'''
    response.headers['Access-Control-Allow-Origin'] = _allow_origin
    response.headers['Access-Control-Allow-Methods'] = _allow_methods
    response.headers['Access-Control-Allow-Headers'] = _allow_headers

@get('/api/v1/version')
def version():
    return dumps({"version":"0.1"})

@get('/api/v1/test')
def test():
    return "test"

@post('/api/v1/test')
def post_test():
    return "test "+request.json['data']

@route('/')
def home():
    redirect('/index.html')

@get('/<filepath:path>')
def serve_static(filepath):
    return static_file(filepath, root='./public/')

if __name__ == "__main__":
	run(app=app, host='0.0.0.0', port=APP_PORT)
