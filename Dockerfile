FROM python:2.7-alpine

RUN apk update

RUN mkdir -p /app/public

COPY ./config.py /app
COPY ./server.py /app
COPY ./requirements.txt /app
COPY ./frontend/dist /app/public
WORKDIR /app

EXPOSE 80

RUN pip install --upgrade -r requirements.txt

CMD ["python", "server.py"]
